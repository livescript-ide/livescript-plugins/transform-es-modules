import
    \assert
    \livescript-compiler/lib/livescript/ast/Node
    \js-nodes/JsNode
    \js-nodes/symbols : { copy, js, as-node }
    \js-nodes/ObjectNode
    \livescript-compiler/lib/livescript/ast/symbols : { parent, type }
    \livescript-compiler/lib/livescript/ast/symbols : { parent, type }
    \livescript-compiler/lib/core/symbols : { init }

export default Import = Node[copy]!

add-extension = (value, extension) ->
    existing-extension = value.match /\.[^.\/]+'$/
    unless existing-extension
        if value.1 in ['.']
            quote-character = value.0
            value.substring(0,value.length - 1) + extension + quote-character
        else
            value
    else
        value

Import[as-node]import-enumerable do
    (type): \Import.ast.livescript

    extension: ''

    (init): (@{names, source,all,inject-to-scope}) ->
        if @extension
            if @source[type] == \Literal
                @source.value = add-extension @source.value, @extension
            else if @source[type] == \Obj
                for item in @source.items
                    item.key.value = add-extension item.key.value, @extension
            else
                console.error "TODO implement adding extension to #{@source[type]}"

    names:~
        -> @_names
        (v) ->
            v?[parent] = @
            @_names = v
    source:~
        -> @_source
        (v) ->
            v?[parent] = @
            @_source = v

    traverse-children: (visitor, cross-scope-boundary) !->
        visitor @names, @, \names  if @names
        visitor @source, @, \source if @source
        @names.traverse-children ...& if @names
        @source.traverse-children ...& if @source

    compile: (o) ->
        names = @names.compile o
        if @all
            @to-source-node parts: [ "import * as ", names, " from ", (@source.compile o), @terminator ]
        else
            @to-source-node parts: [ "import ", names, " from ", (@source.compile o), @terminator ]


    terminator: ';'
