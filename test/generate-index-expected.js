var import$ = (require('./modules/Math')['__default__'] || require('./modules/Math'));
var import1$ = (require('./modules/Vector')['__default__'] || require('./modules/Vector'));
var import2$ = (require('./modules/foo')['__default__'] || require('./modules/foo'));
(function(){
  exports.Math = import$;
  exports.Vector = import1$;
  exports.foo = import2$;
}).call(this);
